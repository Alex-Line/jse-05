package com.iteco.linealex.jse.context;

import com.iteco.linealex.jse.command.*;
import com.iteco.linealex.jse.repository.ProjectRepository;
import com.iteco.linealex.jse.repository.TaskRepository;
import com.iteco.linealex.jse.service.ProjectService;
import com.iteco.linealex.jse.service.TaskService;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public final class Bootstrap {

    private static final Scanner scanner = new Scanner(System.in);

    private ProjectService projectService;

    private TaskService taskService;

    private ProjectRepository projectRepository;

    private TaskRepository taskRepository;

    private Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    {
        registry(new ClearSelectionCommand());
        registry(new ExitCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectListCommand());
        registry(new ProjectRemoveCommand());
        registry(new ProjectSelectCommand());
        registry(new TaskAttachCommand());
        registry(new TaskClearAllCommand());
        registry(new TaskClearCommand());
        registry(new TaskCreateCommand());
        registry(new TaskRemoveCommand());
        registry(new TaskSelectCommand());
        registry(new TaskShowCommand());
    }

    public Bootstrap() {
        this.projectRepository = new ProjectRepository();
        this.taskRepository = new TaskRepository();
        this.taskService = new TaskService(taskRepository);
        this.projectService = new ProjectService(projectRepository);
    }

    private void registry(AbstractCommand command) {
        final String commandName = command.command();
        if (commandName == null || commandName.isEmpty()) return;
        final String commandDescription = command.description();
        if (commandDescription == null || commandDescription.isEmpty()) return;
        command.setBootstrap(this);
        command.setScanner(scanner);
        commands.put(command.command(), command);
    }

    public void start() {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        String command = "";
        while (!"exit".equals(command)) {
            command = scanner.nextLine().trim().toLowerCase();
            execute(command);
        }
    }

    private void execute(final String command) {
        if (command == null || command.isEmpty()) return;
        final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) return;
        try {
            abstractCommand.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public ProjectService getProjectService() {
        return projectService;
    }

    public void setProjectService(ProjectService projectService) {
        this.projectService = projectService;
    }

    public TaskService getTaskService() {
        return taskService;
    }

    public void setTaskService(TaskService taskService) {
        this.taskService = taskService;
    }

    public ProjectRepository getProjectRepository() {
        return projectRepository;
    }

    public void setProjectRepository(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public TaskRepository getTaskRepository() {
        return taskRepository;
    }

    public void setTaskRepository(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public Map<String, AbstractCommand> getCommands() {
        return commands;
    }

    public void setCommands(Map<String, AbstractCommand> commands) {
        this.commands = commands;
    }

}