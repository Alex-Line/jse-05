package com.iteco.linealex.jse.command;

import com.iteco.linealex.jse.entity.Task;

import java.util.Collection;
import java.util.Map;

public class TaskClearAllCommand extends AbstractCommand {

    @Override
    public String command() {
        return "task-clear-all";
    }

    @Override
    public String description() {
        return "REMOVE ALL TASK EVERYWHERE";
    }

    @Override
    public void execute() throws Exception {
        Collection<Task> collection = bootstrap.getTaskService().removeAllTasks();
        if (collection.isEmpty()) {
            System.out.println("[THERE IS NOT ANY TASK]");
            return;
        }
        System.out.println("[ALL TASKS FROM ALL PROJECTS ARE REMOVED]\n");
    }

}
