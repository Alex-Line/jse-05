package com.iteco.linealex.jse.command;

import com.iteco.linealex.jse.entity.Project;
import com.iteco.linealex.jse.util.DateFormatter;
import com.iteco.linealex.jse.util.InsetExistingEntityException;

import java.text.ParseException;

public class ProjectCreateCommand extends AbstractCommand {

    @Override
    public String command() {
        return "project-create";
    }

    @Override
    public String description() {
        return "CREATE A NEW PROJECT AND SET SELECTION ON IT";
    }

    @Override
    public void execute() throws Exception {
        Project project = new Project();
        System.out.println("[ENTER PROJECT NAME]");
        final String projectName = scanner.nextLine().trim();
        project.setName(projectName);
        System.out.println("[ENTER PROJECT DESCRIPTION]");
        final String projectDescription = scanner.nextLine().trim();
        if (projectDescription.equals("")) {
            bootstrap.getProjectService().createProject(projectName);
            return;
        }
        project.setDescription(scanner.nextLine());
        System.out.println("[ENTER PROJECT START DATE IN FORMAT: DD.MM.YYYY]");
        try {
            project.setDateStart(DateFormatter.formatStringToDate(scanner.nextLine().trim()));
        } catch (ParseException e) {
            System.out.println("[WRONG DATE FORMAT! START WILL BE NULL TILL YOU DO NOT CHANGE IT]");
        }
        System.out.println("[ENTER PROJECT FINISH DATE IN FORMAT: DD.MM.YYYY]");
        try {
            project.setDateFinish(DateFormatter.formatStringToDate(scanner.nextLine().trim()));
        } catch (ParseException e) {
            System.out.println("[WRONG DATE FORMAT! FINISH WILL BE NULL TILL YOU DO NOT CHANGE IT]");
        }
        try {
            bootstrap.getProjectService().persist(project);
            System.out.println("[OK]\n");
        } catch (Exception e) {
            System.out.println("[THERE IS ALREADY THE PROJECT WITH NAME: " + project.getName() + "]\n");
        }
    }

}
