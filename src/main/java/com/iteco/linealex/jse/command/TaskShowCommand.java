package com.iteco.linealex.jse.command;

import com.iteco.linealex.jse.entity.Task;

import java.util.Collection;

public class TaskShowCommand extends AbstractCommand {

    @Override
    public String command() {
        return "task-list";
    }

    @Override
    public String description() {
        return "SHOW ALL TASKS IN THE SELECTED PROJECT OR WITHOUT IT";
    }

    @Override
    public void execute() throws Exception {
        Collection<Task> collection = null;
        if (bootstrap.getProjectService().getSelectedProject() != null) {
            collection = bootstrap.getTaskService().getTasks(bootstrap.getProjectService().getSelectedProject().getId());
        } else collection = bootstrap.getTaskService().getTasks();
        if (collection.isEmpty()) {
            System.out.println("[THERE IS NOT ANYTHING TO LIST]\n");
            return;
        }
        System.out.println("[TASK LIST]");
        int index = 1;
        for (Task task : collection) {
            System.out.println(index + ". " + task);
            index++;
        }
        System.out.println();
    }

}
