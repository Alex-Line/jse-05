package com.iteco.linealex.jse.command;

import com.iteco.linealex.jse.entity.Task;

public class TaskSelectCommand extends AbstractCommand {

    @Override
    public String command() {
        return "task-select";
    }

    @Override
    public String description() {
        return "SELECT A TASK FOR MANIPULATION FURTHER";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[ENTER THE NAME OF TASK FOR SELECTION]");
        final String taskName = scanner.nextLine().trim();
        Task task = null;
        if (bootstrap.getProjectService().getSelectedProject() != null) {
            task = bootstrap.getTaskService().selectTask(bootstrap.getProjectService().getSelectedProject().getId(),
                    taskName);
        } else task = bootstrap.getTaskService().selectTask(taskName);
        if (task == null) {
            System.out.println("[THERE IS NOT SUCH TASK AS " + taskName
                    + ". PLEASE TRY TO SELECT AGAIN OR SELECT PROJECT FIRST]\n");
            return;
        }
        if (task.getProjectId() == null) {
            System.out.println("[WAS SELECTED WITHOUT PROJECT]");
            System.out.println(task + "\n");
        }
        System.out.println("[WAS SELECTED IN THE PROJECT WITH ID" + task.getProjectId() + "]");
        System.out.println(task + "\n");
    }

}
