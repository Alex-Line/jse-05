package com.iteco.linealex.jse.command;

import com.iteco.linealex.jse.entity.Project;

public class TaskAttachCommand extends AbstractCommand {
    @Override
    public String command() {
        return "task-attach";
    }

    @Override
    public String description() {
        return "ATTACH THE SELECTED TASK TO PROJECT";
    }

    @Override
    public void execute() throws Exception {
        if (bootstrap.getTaskService().getSelectedTask() == null) {
            System.out.println("[YOU DID NOT SELECT ANY TASK! SELECT ANY AND TRY AGAIN]\n");
            return;
        }
        System.out.println("[ENTER PROJECT NAME]");
        final String projectName = scanner.nextLine().trim();
        Project project = bootstrap.getProjectService().getProjectByName(projectName);
        boolean result = bootstrap.getTaskService().attachTaskToProject(project.getId());
        if (result) System.out.println("[OK]\n");
        else System.out.println("THERE IS NOT SUCH PROJECT! PLEASE TRY AGAIN!\n");
    }
}
