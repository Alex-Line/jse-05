package com.iteco.linealex.jse.command;

import com.iteco.linealex.jse.entity.Task;

import java.util.Collection;
import java.util.Map;

public class TaskClearCommand extends AbstractCommand {

    @Override
    public String command() {
        return "task-clear";
    }

    @Override
    public String description() {
        return "REMOVE ALL TASKS FROM THE SELECTED PROJECT";
    }

    @Override
    public void execute() throws Exception {
        Collection<Task> collection = null;
        if (bootstrap.getProjectService().getSelectedProject() != null) {
            collection = bootstrap.getTaskService().removeAllTasksFromProject(
                    bootstrap.getProjectService().getSelectedProject().getId());
        }
        if (collection.isEmpty()) {
            System.out.println("[YOU DID NOT SELECT ANY PROJECT! SELECT ANY AND TRY AGAIN]\n");
            return;
        }
        System.out.println("[REMOVING TASKS FROM PROJECT]");
        System.out.println("[ALL TASKS REMOVED]");
        System.out.println("[OK]\n");
    }

}
