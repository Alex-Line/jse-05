package com.iteco.linealex.jse.command;

public class ExitCommand extends AbstractCommand {

    @Override
    public String command() {
        return "exit";
    }

    @Override
    public String description() {
        return "EXIT FROM APPLICATION";
    }

    @Override
    public void execute() throws Exception {
        System.exit(0);
    }

}
