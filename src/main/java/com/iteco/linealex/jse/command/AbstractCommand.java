package com.iteco.linealex.jse.command;

import com.iteco.linealex.jse.context.Bootstrap;

import java.text.ParseException;
import java.util.Scanner;

public abstract class AbstractCommand {

    protected Bootstrap bootstrap;

    protected Scanner scanner;

    public abstract String command();

    public abstract String description();

    public abstract void execute() throws Exception;

    public Bootstrap getBootstrap() {
        return bootstrap;
    }

    public void setBootstrap(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public Scanner getScanner() {
        return scanner;
    }

    public void setScanner(Scanner scanner) {
        this.scanner = scanner;
    }

}
