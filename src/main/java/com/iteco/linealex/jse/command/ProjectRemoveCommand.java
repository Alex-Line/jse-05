package com.iteco.linealex.jse.command;

import com.iteco.linealex.jse.entity.Project;
import com.iteco.linealex.jse.entity.Task;

import java.util.Collection;
import java.util.Map;

public class ProjectRemoveCommand extends AbstractCommand {

    @Override
    public String command() {
        return "project-remove";
    }

    @Override
    public String description() {
        return "REMOVING A PROJECT BY NAME";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[ENTER PROJECT NAME]");
        final String projectName = scanner.nextLine().trim();
        Project project = bootstrap.getProjectService().removeProject(projectName);
        if (project == null) System.out.println("[THERE IS NOT SUCH PROJECT! PLEASE TRY AGAIN!]\n");
        System.out.println("[REMOVING PROJECT]");
        Collection<Task> collection = bootstrap.getTaskService().removeAllTasksFromProject(projectName);
        if (collection != null) {
            System.out.println("[REMOVING TASKS FROM PROJECT]");
            System.out.println("[ALL TASKS REMOVED]");
        }
        System.out.println("[OK]\n");
    }

}
