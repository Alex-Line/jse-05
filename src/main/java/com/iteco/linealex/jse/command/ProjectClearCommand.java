package com.iteco.linealex.jse.command;

import com.iteco.linealex.jse.entity.Project;

import java.util.Collection;
import java.util.Map;

public class ProjectClearCommand extends AbstractCommand {

    @Override
    public String command() {
        return "project-clear";
    }

    @Override
    public String description() {
        return "CLEAR THE LIST OF PROJECTS";
    }

    @Override
    public void execute() throws Exception {
        Collection<Project> collection = bootstrap.getProjectService().removeAllProjects();
        if (collection == null) return;
        System.out.println("[All PROJECTS REMOVED]\n");
    }

}
