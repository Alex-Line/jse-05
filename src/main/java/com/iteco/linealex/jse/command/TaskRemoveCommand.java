package com.iteco.linealex.jse.command;

import com.iteco.linealex.jse.entity.Task;

public class TaskRemoveCommand extends AbstractCommand {

    @Override
    public String command() {
        return "task-remove";
    }

    @Override
    public String description() {
        return "REMOVE ONE TASK IN SELECTED PROJECT OR WITHOUT IT";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[ENTER TASK NAME]");
        final String taskName = scanner.nextLine().trim();
        System.out.println("REMOVING TASK...");
        Task task = null;
        if (bootstrap.getProjectService().getSelectedProject() != null) {
            task = bootstrap.getTaskService().removeTask(bootstrap.getProjectService().getSelectedProject().getId(),
                    taskName);
        } else task = bootstrap.getTaskService().removeTask(taskName);
        if (task == null) {
            System.out.println("[THERE IS NOT SUCH TASK AS " + taskName
                    + ". PLEASE TRY AGAIN OR SELECT PROJECT FIRST]\n");
            return;
        }
        System.out.println("[OK]\n");
    }

}
