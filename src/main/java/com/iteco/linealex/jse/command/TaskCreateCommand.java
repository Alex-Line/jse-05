package com.iteco.linealex.jse.command;

import com.iteco.linealex.jse.entity.Task;

public class TaskCreateCommand extends AbstractCommand {

    @Override
    public String command() {
        return "task-create";
    }

    @Override
    public String description() {
        return "CREATE A NEW TASK IN PROJECT OR WITHOUT IT";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[ENTER TASK NAME]");
        final String taskName = scanner.nextLine().trim();
        Task task = null;
        if (bootstrap.getProjectService().getSelectedProject() != null) {
            task = bootstrap.getTaskService().createTask(bootstrap.getProjectService().getSelectedProject().getId(),
                    taskName);
        } else task = bootstrap.getTaskService().createTask(taskName);
        if (task == null) {
            System.out.println("[THERE IS SUCH TASK ALREADY]\n");
            return;
        }
        System.out.println("[OK]\n");
    }

}
