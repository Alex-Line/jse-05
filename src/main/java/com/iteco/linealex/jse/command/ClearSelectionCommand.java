package com.iteco.linealex.jse.command;

public class ClearSelectionCommand extends AbstractCommand {

    @Override
    public String command() {
        return "selection-clear";
    }

    @Override
    public String description() {
        return "RESET SELECTION TASK AND PROJECT";
    }

    @Override
    public void execute() throws Exception {
        bootstrap.getProjectService().clearSelection();
        bootstrap.getTaskService().clearSelection();
        System.out.println("[ALL SELECTION WAS CANCELED]\n");
    }
}
