package com.iteco.linealex.jse.command;

import com.iteco.linealex.jse.entity.Project;

import java.util.Collection;

public class ProjectListCommand extends AbstractCommand {

    @Override
    public String command() {
        return "project-list";
    }

    @Override
    public String description() {
        return "SHOW ALL PROJECTS";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT LIST]");
        Collection<Project> collection = bootstrap.getProjectService().getAllProject();
        if (collection == null) {
            System.out.println("[THERE IS NOT ANY PROJECTS]");
            return;
        }
        int index = 1;
        for (Project project : collection) {
            System.out.println(index + ". " + project);
            index++;
        }
        System.out.println();
    }

}
