package com.iteco.linealex.jse.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateFormatter {

    private static SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");

    public static String formatDateToString(Date date) {
        return format.format(date);
    }

    public static Date formatStringToDate(String stringDate) throws ParseException {
        return format.parse(stringDate);
    }

}
